! https://github.com/gotbletu/shownotes/blob/master/xterm_xresources.md
!------------------------------------------------------
! https://wiki.archlinux.org/index.php/Xterm
! https://lukas.zapletalovi.com/2013/07/hidden-gems-of-xterm.html
! http://www.futurile.net/2016/06/14/xterm-setup-and-truetype-font-configuration/
! http://www.futurile.net/2016/06/15/xterm-256color-themes-molokai-terminal-theme/

! Allow xterm to report the TERM variable correctly.
! Do not set the TERM variable from your ~/.bashrc or ~/.bash_profile or similar file.
! The terminal itself should report the correct TERM to the system so that the proper terminfo file will be used.
! Two usable terminfo names are xterm and xterm-256color
XTerm.termName: xterm-256color

! Fonts ====================================================
! set font and fontsize
XTerm*faceName: DejaVu Sans Mono
XTerm*faceSize: 20

! VT Font Menu: Unreadable
xterm*faceSize1: 8
! VT font menu: Tiny
xterm*faceSize2: 10
! VT font menu: Medium
xterm*faceSize3: 12
! VT font menu: Large
xterm*faceSize4: 16
! VT font menu: Huge
xterm*faceSize5: 22

! Ensure that your locale is set up for UTF-8. If you do not use UTF-8, you may need to force xterm to more strictly follow your locale by setting
XTerm.vt100.locale: true

! Cursor ====================================================
! pointer and cursor (blinking and color)
XTerm*pointerColor: white
XTerm*pointerColorBackground: black
XTerm*cursorColor: white
XTerm*cursorBlink: true

!! Keybinding ========================================================
! http://blog.rot13.org/2010/03/change-font-size-in-xterm-using-keyboard.html
! - change fontsize on the fly (ctrl+plus = increase ; ctrl+minus = decrease, ctrl+0 = default)
! - copy/paste hotkey (ctrl+shift+c = copy ; ctrl+shift+v = paste)
! - open url (clickable links)
!   1) double click to highlight the full url
!   2) Shift + click it to open it
XTerm.vt100.translations: #override \n\
  Ctrl Shift <Key>M: smaller-vt-font() \n\
  Ctrl Shift <Key>P: larger-vt-font() \n\
  Ctrl Shift <Key> 0: set-vt-font(d) \n\
  Ctrl Shift <Key>C: copy-selection(CLIPBOARD) \n\
  Ctrl Shift <Key>V: insert-selection(CLIPBOARD) \n\
  Shift <Btn1Up>: exec-formatted("xdg-open '%t'", PRIMARY) \n\
  <Btn1Up>: select-end(PRIMARY, CLIPBOARD, CUT_BUFFER0) \n\
  <Btn2Up>: insert-selection(PRIMARY)


!!Source http://github.com/altercation/solarized

*background: #002b36
*foreground: #657b83
!!*fading: 40
*fadeColor: #002b36
*cursorColor: #93a1a1
*pointerColorBackground: #586e75
*pointerColorForeground: #93a1a1

!! black dark/light
*color0: #073642
*color8: #002b36

!! red dark/light
*color1: #dc322f
*color9: #cb4b16

!! green dark/light
*color2: #859900
*color10: #586e75

!! yellow dark/light
*color3: #b58900
*color11: #657b83

!! blue dark/light
*color4: #268bd2
*color12: #839496

!! magenta dark/light
*color5: #d33682
*color13: #6c71c4

!! cyan dark/light
*color6: #2aa198
*color14: #93a1a1

!! white dark/light
*color7: #eee8d5
*color15: #fdf6e3
