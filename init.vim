"http://michaelabrahamsen.com/posts/replace-tmux-with-neovim/
set nocompatible

let $NVIM_TUI_ENABLE_CURSOR_SHAPE = 0

call plug#begin()
  Plug 'itchyny/lightline.vim'
  Plug 'sjl/gundo.vim'
  Plug 'rking/ag.vim'
  Plug 'glts/vim-radical'
  Plug 'glts/vim-magnum'
  Plug 'moll/vim-bbye'
  Plug '~/.fzf'
  Plug 'junegunn/fzf.vim'
  Plug 'gcmt/taboo.vim'
  Plug 'drewtempelmeyer/palenight.vim'
  Plug 'ggVGc/vim-fuzzysearch'
  Plug 'ap/vim-css-color'
  Plug 'mattn/emmet-vim'
  Plug 'Valloric/MatchTagAlways'
  Plug 'tpope/vim-fugitive'
  Plug 'airblade/vim-gitgutter'
  Plug 'francoiscabrol/ranger.vim'
  Plug 'vim-scripts/coco'
  Plug 'neoclide/coc.nvim', {'branch': 'release'}
  Plug 'vim-scripts/DoxygenToolkit.vim'
  Plug 'ayu-theme/ayu-vim'
  Plug 'octol/vim-cpp-enhanced-highlight'
  Plug 'ptzz/lf.vim'
  Plug 'rbgrouleff/bclose.vim'
call plug#end()

filetype on
syntax on
syntax enable

set cursorline

set termguicolors

let ayucolor="mirage"
"set background=dark
silent! colorscheme ayu
"let g:palenight_terminal_italics=1

set number

let mapleader=" "

nnoremap <Leader>s :source $MYVIMRC<CR>

set hidden

" Close the current buffer and move to the previous one
" This replicates the idea of closing a tab
:nnoremap <Leader>q :Bdelete!<CR>

set history=100

set showcmd       " display incomplete commands
set incsearch     " do incremental searching
nnoremap * *N
set laststatus=2  " Always display the status line

" Display extra whitespace
set list listchars=tab:»·,trail:·,nbsp:·

set backspace=indent,eol,start

filetype indent on
set nowrap
set tabstop=2
set shiftwidth=2
set expandtab
set smartindent
set autoindent
set ruler
set relativenumber
autocmd BufWritePre * :%s/\s\+$//e


set hlsearch
nnoremap <silent> <Esc> :nohlsearch<Bar>:echo<CR>

nnoremap <Leader><Leader> :e#<CR>

" move vertically by visual line
nnoremap j gj
nnoremap k gk

nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" fd is escape
inoremap fd <esc>

set splitbelow
set splitright

set showmatch

set wildignore+=*.log,*.sql,*.cache,*.dup,*/.git/*,*/.hg/*,*/.svn/*,*/.idea/*,*/.DS_Store,*/vendor

" toggle gundo
nnoremap <leader>u :GundoToggle<CR>

" open ag.vim
nnoremap <leader>a :Ag

set updatecount =100
set undofile

let g:rustfmt_autosave = 1
" Avoids updating the screen before commands are completed
set lazyredraw

" Remap navigation commands to center view on cursor using zz
nnoremap <C-U> 11kzz
nnoremap <C-D> 11jzz
nnoremap j jzz
nnoremap k kzz
nnoremap # #zz
nnoremap * *zz
nnoremap n nzz
nnoremap N Nzz

:au BufEnter * if &buftype == 'terminal' | endif
:tnoremap fd <C-\><C-n>
:tnoremap <expr> <C-R> '<C-\><C-N>"'.nr2char(getchar()).'pi'
:tnoremap <C-h> <C-\><C-N><C-w>h
:tnoremap <C-j> <C-\><C-N><C-w>j
:tnoremap <C-k> <C-\><C-N><C-w>k
:tnoremap <C-l> <C-\><C-N><C-w>l
:inoremap <C-h> <C-\><C-N><C-w>h
:inoremap <C-j> <C-\><C-N><C-w>j
:inoremap <C-k> <C-\><C-N><C-w>k
:inoremap <C-l> <C-\><C-N><C-w>l
:nnoremap <C-h> <C-w>h
:nnoremap <C-j> <C-w>j
:nnoremap <C-k> <C-w>k
:nnoremap <C-l> <C-w>l

" Go to tab by number
noremap <leader>1 1gt
noremap <leader>2 2gt
noremap <leader>3 3gt
noremap <leader>4 4gt
noremap <leader>5 5gt
noremap <leader>6 6gt
noremap <leader>7 7gt
noremap <leader>8 8gt
noremap <leader>9 9gt
noremap <leader>0 :tablast<cr>
let g:taboo_tab_format = " %N %f%m "

function! Openterm(name)
   enew
   :call termopen(join(['/usr/bin/zsh #', a:name]))
   :set modifiable
   startinsert
endfunction
command! -nargs=* T :call Openterm(<q-args>)

nnoremap / :FuzzySearch<CR>
let g:fuzzysearch_prompt = '/'

let g:fzf_layout = { 'down': '~40%' }

" Customize fzf colors to match your color scheme
let g:fzf_colors =
\ { 'fg':      ['fg', 'Normal'],
  \ 'bg':      ['bg', 'Normal'],
  \ 'hl':      ['fg', 'Comment'],
  \ 'fg+':     ['fg', 'CursorLine', 'CursorColumn', 'Normal'],
  \ 'bg+':     ['bg', 'CursorLine', 'CursorColumn'],
  \ 'hl+':     ['fg', 'Statement'],
  \ 'info':    ['fg', 'PreProc'],
  \ 'border':  ['fg', 'Ignore'],
  \ 'prompt':  ['fg', 'Conditional'],
  \ 'pointer': ['fg', 'Exception'],
  \ 'marker':  ['fg', 'Keyword'],
  \ 'spinner': ['fg', 'Label'],
  \ 'header':  ['fg', 'Comment'] }


nnoremap <leader>e :Files<CR>
nnoremap <leader>b :Buffers<CR>

inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"

nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)
nmap <silent> C-] <Plug>(coc-definition)
nmap <silent> C-^ <Plug>(coc-references)

" gh - get hint on whatever's under the cursor
nnoremap <silent> gh :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if &filetype == 'vim'
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

" Highlight symbol under cursor on CursorHold
autocmd CursorHold * silent call CocActionAsync('highlight')

nnoremap <silent> <leader>co  :<C-u>CocList outline<cr>
nnoremap <silent> <leader>cs  :<C-u>CocList -I symbols<cr>

" view all errors
nnoremap <silent> <leader>cl  :<C-u>CocList locationlist<CR>

" manage extensions
nnoremap <silent> <leader>cx  :<C-u>CocList extensions<cr>

" rename the current word in the cursor
nmap <leader>cr  <Plug>(coc-rename)
nmap <leader>cf  <Plug>(coc-format-selected)
vmap <leader>cf  <Plug>(coc-format-selected)

set cmdheight=2
set shortmess+=c
set signcolumn=yes

let g:lightline = {
      \ 'colorscheme': 'ayu',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'cocstatus', 'readonly', 'filename', 'modified' ] ]
      \ },
      \ 'component_function': {
      \   'cocstatus': 'coc#status'
      \ },
      \ }



""inoremap " ""<left>
""inoremap ' ''<left>
""inoremap ( ()<left>
""inoremap [ []<left>
""inoremap { {}<left>
""inoremap {<CR> {<CR>}<ESC>O
""inoremap {;<CR> {<CR>};<ESC>O

map <C-c> :call CocoComment()<CR>
map <C-u> :call CocoUncomment()<CR>
map <C-t> :call CocoToggle()<CR>

let g:bclose_no_plugin_maps = 1
